My coursework for the University of Abertay CGAD/CGT course AG0904A DirectX Graphics.

##Development

Based on the [RasterTek Framework](http://rastertek.com/tutdx11.html). Latest version can be found on the Toon Branch.

##Features
* Two "modes": _Normal vision_, and _Lecturer vision_.
* _Normal Vision_ features realistic lighting, using bump map textures to give objects more detail.
* Realistic glass created through use of reflection, refraction and bump map shaders.
* _Lecturer Vision_ features a [Cel Shaded](http://en.wikipedia.org/wiki/Cel_shading) world; particle effects to represent the hair of the lecturer; and a moving dragon which casts a shadow on the terrain.
* Terrain is generated through a height map.
* Models which are loaded in can either be in [Wavefront .obj](http://en.wikipedia.org/wiki/Wavefront_.obj_file) format, or the [RasterTek custom](http://rastertek.com/dx11tut07.html) format (.txt)

##Screenshots
![Before Toon Shader](http://i.imgur.com/JjWoBqK.png)

![After Toon Shader](http://i.imgur.com/qUqk9F2.png)

![Glass](http://i.imgur.com/QfHg7G5.png)

![Dragon with Shadow](http://i.imgur.com/0BCk5G2.png)


##Feedback
_Grade_: 19.00 out of 20.00

_Comments_: overall an excellent coursework. Toon shader is great. Almost everything that was asked for. Working shadows etc. presentation was very good, good explanation and demonstration of work.