//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef TEXTURE_H
#define TEXTURE_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include <d3dx11tex.h>

class CTexture
{
public:
	CTexture();
	~CTexture();
private:
	// disallow copying and moving
	CTexture(const CTexture& other);
	CTexture(CTexture&& other);
	CTexture& operator= (const CTexture& other);
	CTexture& operator= (CTexture&& other);

public:
	ukl::UKLResult initialise(ID3D11Device *device, WCHAR *filename);
	void terminate();

	ID3D11ShaderResourceView *getTexture();

private:
	ID3D11ShaderResourceView *m_texture;
};

#endif //! TEXTURE_H