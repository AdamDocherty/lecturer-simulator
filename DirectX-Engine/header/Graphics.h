//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef GRAPHICS_H
#define GRAPHICS_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include "BumpMapShader.h"
#include "Camera.h"
#include "D3D.h"
#include "DebugWindow.h"
#include "Light.h"
#include "LightShader.h"
#include "Model.h"
#include "OrthoWindow.h"
#include "../OutlineShader.h"
#include "Skybox.h"
#include "ReflectionShader.h"
#include "RenderTexture.h"
#include "Terrain.h"
#include "TerrainShader.h"
#include "TessellationShader.h"
#include "Text.h"
#include "TextureShader.h"
#include "ToonShader.h"

const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 1000.f;
const float SCREEN_NEAR = 0.1f;

class CGraphics
{
public:
	CGraphics();
	~CGraphics();
private:
	// disallow copying and moving
	CGraphics(const CGraphics& other);
	CGraphics(CGraphics&& other);
	CGraphics& operator= (const CGraphics& other);
	CGraphics& operator= (CGraphics&& other);

public:
	ukl::UKLResult initialise(CCamera *camera, int screenWidth, int screenHeight, HWND hwnd);
	void terminate();
	ukl::UKLResult frame(CCamera *camera, int fps, int cpuUsage, float timeElapsed);

	CD3D *getD3D() { return m_d3d; }
public:
	ukl::UKLResult render();

private:
	ukl::UKLResult drawLoadingScreen();

	ukl::UKLResult renderSceneToTexture();
	ukl::UKLResult renderScene();
	ukl::UKLResult Render2DTextureScene();

	ukl::UKLResult renderToWaterReflection();
	ukl::UKLResult renderToWaterRefraction();

	ukl::UKLResult renderToFloorReflection();

	ukl::UKLResult renderToDebugWindow();

private:
	CCamera				*m_camera;
	CD3D				*m_d3d;
	CLight				*m_light;
	CText				*m_text;
	CSkybox				*m_skybox;
	CDebugWindow		*m_fullScreenWindow; 

	CTexture			*m_blurTerrainTexture;

	// Shaders
	CTextureShader		*m_textureShader;
	CLightShader		*m_lightShader;
	CBumpMapShader		*m_bumpMapShader;
	CReflectionShader	*m_reflectionShader;
	CTerrainShader		*m_terrainShader;
	CTessellationShader *m_tessellationShader;
	CToonShader			*m_toonShader;
	COutlineShader      *m_outlineShader;

	// Generic Models
	CModel				*m_model;
	CModel				*m_wallModel;

	// Render Textures
	CRenderTexture		*m_sceneRenderTexture;
	
	// Terrain Model
	CTerrain			*m_terrain;

	// Debug Window
	CDebugWindow		*m_debugWindow;
	CRenderTexture		*m_debugWindowTexture;

	// Reflective Floor
	CModel				*m_floorModel;
	CRenderTexture		*m_floorReflectionTexture;

	// Water
	CModel				*m_waterModel;
	CRenderTexture		*m_waterReflectionTexture;
	CRenderTexture		*m_waterRefractionTexture;
	float				m_waterHeight; 
	float				m_waterTranslation;

	float				m_rotation;
	D3DXMATRIX			m_baseViewMatrix;

	bool				m_paulVision;
};

#endif //! GRAPHICS_H