//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef TEXT_H
#define TEXT_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include <vector>

#include "Font.h"
#include "FontShader.h"

class CText
{
	struct SentenceType
	{
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};
public:
	CText();
	~CText();
private:
	// disallow copying and moving
	CText(const CText& other);
	CText(CText&& other);
	CText& operator= (const CText& other);
	CText& operator= (CText&& other);

public:
	ukl::UKLResult initialise(ID3D11Device*, ID3D11DeviceContext*, HWND, int, int, D3DXMATRIX);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX orthoMatrix);

	ukl::UKLResult updateText(unsigned int index, char* text, int x, int y, 
		float r, float g, float b, ID3D11DeviceContext* deviceContext);

private:
	ukl::UKLResult initialiseSentence(SentenceType** sentence, int maxLength, ID3D11Device* device);
	ukl::UKLResult updateSentence(SentenceType* sentence, char* text, int positionX, int positionY, float 
		red, float green, float blue, ID3D11DeviceContext* deviceContext);
	void releaseSentence(SentenceType**);
	ukl::UKLResult renderSentence(ID3D11DeviceContext*, SentenceType*, D3DXMATRIX, D3DXMATRIX);

private:
	CFont* m_font;
	CFontShader* m_fontShader;
	int m_screenWidth, m_screenHeight;
	D3DXMATRIX m_baseViewMatrix;

	std::vector<SentenceType*> m_sentenceArray;

	//SentenceType* m_sentence1;
	//SentenceType* m_sentence2;

};

#endif //! TEXT_H