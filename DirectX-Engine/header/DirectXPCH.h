//
// Author: Adam Docherty
// Date:   2013-11-26
//

#ifndef DIRECTX_PCH_H
#define DIRECTX_PCH_H

//_____________________________________________________________________________
// Standard Header file for the project, include any files that are used
// frequently, but are changed infrequently.
//

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WindowsX.h>

#include <dxgi.h>
#include <D3DCommon.h>
#include <d3d11.h>
#include <d3dx10math.h>

#include "UKL.h"

#endif //! DIRECTX_PCH_H