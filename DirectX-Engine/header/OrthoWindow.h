#ifndef ORTHO_WINDOW_H
#define ORTHO_WINDOW_H

#include "DirectXPCH.h"

class COrthoWindow
{
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	public:
	COrthoWindow();
	~COrthoWindow();

	ukl::UKLResult initialise(ID3D11Device*, int, int);
	void terminate();
	void render(ID3D11DeviceContext*);

	int getIndexCount();

private:
	ukl::UKLResult initialiseBuffers(ID3D11Device*, int, int);
	void terminateBuffers();
	void renderBuffers(ID3D11DeviceContext*);

private:

	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

};

#endif//! ORTHO_WINDOW_H