//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef FONT_H
#define FONT_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"
#include <fstream>

#include "Texture.h"

class CFont
{
	struct FontType
	{
		float left, right;
		int size;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

public:
	CFont();
	~CFont();
private:
	// disallow copying and moving
	CFont(const CFont& other);
	CFont(CFont&& other);
	CFont& operator= (const CFont& other);
	CFont& operator= (CFont&& other);

public:
	ukl::UKLResult initialise(ID3D11Device*, char*, WCHAR*);
	void terminate();

	ID3D11ShaderResourceView* getTexture();

	void buildVertexArray(void*, char*, float, float);

private:
	ukl::UKLResult loadFontData(char*);
	void releaseFontData();
	ukl::UKLResult loadTexture(ID3D11Device*, WCHAR*);
	void releaseTexture();

private:
	FontType* m_font;
	CTexture* m_texture;
};

#endif //! FONT_H