//
// Author: Adam Docherty
// Date:   06-11-2013
//

#ifndef BUMP_MAP_SHADER_H
#define BUMP_MAP_SHADER_H

//_____________________________________________________________________________
//

#include "DirectXPCH.h"
#include <d3dx11async.h>
#include <fstream>

class CBumpMapShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct LightBufferType
	{
		D3DXVECTOR4 ambientColour;
		D3DXVECTOR4 diffuseColor;
		D3DXVECTOR3 lightDirection;
		float padding;
	};

public:
	CBumpMapShader();
	~CBumpMapShader();
private:
	// disallow copying and moving
	CBumpMapShader(const CBumpMapShader& other);
	CBumpMapShader(CBumpMapShader&& other);
	CBumpMapShader& operator= (const CBumpMapShader& other);
	CBumpMapShader& operator= (CBumpMapShader&& other);
public:

	ukl::UKLResult initialise(ID3D11Device*, HWND);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
				D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView** textureArray, D3DXVECTOR3 lightDirection,
				D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor);

private:
	ukl::UKLResult initialiseShader(ID3D11Device*, HWND, WCHAR*, WCHAR*);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
				D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView** textureArray, D3DXVECTOR3 lightDirection,
				D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor);
	void renderShader(ID3D11DeviceContext*, int);

private:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* m_lightBuffer;
};

#endif //! BUMP_MAP_SHADER_H
