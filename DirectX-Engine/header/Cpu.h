//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef CPU_H
#define CPU_H

//______________________________________________________________________________
//

#pragma comment(lib, "pdh.lib")

#include <pdh.h>
#include "DirectXPCH.h"

class CCpu
{
public:
	CCpu();
	~CCpu();
private:
	// disallow copying and moving
	CCpu(const CCpu& other);
	CCpu(CCpu&& other);
	CCpu& operator= (const CCpu& other);
	CCpu& operator= (CCpu&& other);

public:
	ukl::UKLResult initialise();
	void terminate();
	void frame();
	int getCpuPercentage();

private:
	bool m_canReadCpu;
	HQUERY m_queryHandle;
	HCOUNTER m_counterHandle;
	unsigned long m_lastSampleTime;
	long m_cpuUsage;
};

#endif //! CPU_H