// Author: Adam Docherty
// Date: 2013-11-21

#ifndef WATER_SHADER_H
#define WATER_SHADER_H

#include "UKL.h"

#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>

class CWaterShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct ReflectionBufferType
	{
		D3DXMATRIX reflection;
	};

	struct WaterBufferType
	{
		float waterTranslation;
		float reflectRefractScale;
		D3DXVECTOR2 padding;
	};

public:
	CWaterShader();
	~CWaterShader();

	ukl::UKLResult initialise(ID3D11Device*, HWND);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, 
		    ID3D11ShaderResourceView*, ID3D11ShaderResourceView*, float, float);

private:
	ukl::UKLResult initialiseShader(ID3D11Device*, HWND, WCHAR*, WCHAR*);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*,
				 ID3D11ShaderResourceView*, ID3D11ShaderResourceView*, float, float);
	void renderShader(ID3D11DeviceContext*, int);

private:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* m_matrixBuffer;

	ID3D11Buffer* m_reflectionBuffer;
	ID3D11Buffer* m_waterBuffer;
};

#endif //! WATER_SHADER_H