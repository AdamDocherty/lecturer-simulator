//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef INPUT_H
#define INPUT_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

namespace MouseButtons
{
	enum MouseButton {
		LEFT = 0,
		RIGHT = 1,
		MIDDLE = 2,
		NUM_MOUSE_BUTTONS = 3
	};
};

typedef MouseButtons::MouseButton MouseButton;

class CInput
{
public:
	CInput();
	~CInput();
private:
	// disallow copying and moving
	CInput(const CInput& other);
	CInput(CInput&& other);
	CInput& operator= (const CInput& other);
	CInput& operator= (CInput&& other);

public:
	ukl::UKLResult initialise(HWND hwnd);
	void refresh();

	void keyDown(unsigned int key);
	void keyUp(unsigned int key);

	void buttonDown(MouseButton button);
	void buttonUp(MouseButton button);

	void setMousePos(int x, int y);

	bool isKeyDown(unsigned int key);
	bool isKeyPressed(unsigned int key);
	bool isKeyReleased(unsigned int key);

	bool isButtonDown(MouseButton button);
	bool isButtonPressed(MouseButton button);
	bool isButtonReleased(MouseButton button);

	void getMousePos(int *x, int *y);

private:
	static const unsigned NUM_KEYS = 256;
	
	HWND		m_hwnd;

	bool m_keys[NUM_KEYS];
	bool m_keyPressed[NUM_KEYS];
	bool m_keyReleased[NUM_KEYS];

	bool m_mouseButtons[MouseButtons::NUM_MOUSE_BUTTONS];
	bool m_mouseButtonPressed[MouseButtons::NUM_MOUSE_BUTTONS];
	bool m_mouseButtonReleased[MouseButtons::NUM_MOUSE_BUTTONS];
};

#endif //! INPUT_H