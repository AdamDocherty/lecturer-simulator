#ifndef _TERRAINCLASS_H_
#define _TERRAINCLASS_H_


#include "DirectXPCH.h"

#include <stdio.h>

#include "Texture.h"

const int TEXTURE_REPEAT = 8;

class CTerrain
{
private:
		struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
	};

	struct HeightMapType 
	{ 
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	struct VectorType 
	{ 
		float x, y, z;
	};

public:
	CTerrain();
	~CTerrain();

	ukl::UKLResult initialise(ID3D11Device*, char*, WCHAR*);
	void terminate();
	void render(ID3D11DeviceContext*);

	int getIndexCount();

	ID3D11ShaderResourceView* getTexture();
	D3DXMATRIX *getMatrix() { return &m_matrix; }

private:

	ukl::UKLResult loadHeightMap(char*);
	void normaliseHeightMap();
	ukl::UKLResult calculateNormals();
	void terminateHeightMap();

	void calculateTextureCoordinates();
	ukl::UKLResult loadTexture(ID3D11Device*, WCHAR*);
	void releaseTexture();

	ukl::UKLResult initialiseBuffers(ID3D11Device*);
	void terminateBuffers();
	void renderBuffers(ID3D11DeviceContext*);


private:
	static const float NORMALISE_VALUE;

	int m_terrainWidth, m_terrainHeight;
	int m_vertexCount, m_indexCount;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	CTexture* m_texture;
	HeightMapType* m_heightMap;
	D3DXMATRIX m_matrix;
};

#endif