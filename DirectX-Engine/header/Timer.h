//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef TIMER_H
#define TIMER_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

class CTimer
{
public:
	CTimer();
	~CTimer();
private:
	// disallow copying and moving
	CTimer(const CTimer& other);
	CTimer(CTimer&& other);
	CTimer& operator= (const CTimer& other);
	CTimer& operator= (CTimer&& other);

public:
	ukl::UKLResult initialise();
	void frame();

	float getTime();

private:
	INT64 m_frequency;
	float m_ticksPerMs;
	INT64 m_startTime;
	float m_frameTime;
};

#endif //! TIMER_H