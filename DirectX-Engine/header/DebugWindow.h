#ifndef DEBUG_WINDOW_H
#define DEBUG_WINDOW_H

#include "DirectXPCH.h"

class CDebugWindow
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

public:
	CDebugWindow();
	~CDebugWindow();
private:
	// disallow copying and moving
	CDebugWindow(const CDebugWindow& other);
	CDebugWindow(CDebugWindow&& other);
	CDebugWindow& operator= (const CDebugWindow& other);
	CDebugWindow& operator= (CDebugWindow&& other);
public:

	ukl::UKLResult initialise(ID3D11Device* device, int screenWidth, int screenHeight, int bitmapWidth, int bitmapHeight);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext* deviceContext, int positionX, int positionY);

	int getIndexCount();

private:
	ukl::UKLResult initialiseBuffers(ID3D11Device* device);
	void terminateBuffers();
	ukl::UKLResult updateBuffers(ID3D11DeviceContext* deviceContext, int positionX, int positionY);
	void renderBuffers(ID3D11DeviceContext*);

private:
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	int m_screenWidth, m_screenHeight;
	int m_bitmapWidth, m_bitmapHeight;
	int m_previousPosX, m_previousPosY;
};

#endif //! DEBUG_WINDOW_H
