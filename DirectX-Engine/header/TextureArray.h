//
// Author: Adam Docherty
// Date 06-11-2013
//

#ifndef TEXTURE_ARRAY_H
#define TEXTURE_ARRAY_H

//_____________________________________________________________________________
//

#include "DirectXPCH.h"

#include <d3dx11tex.h>

class CTextureArray
{
public:
	CTextureArray(unsigned int size);
	~CTextureArray();
private:
	// disallow copying and moving
	CTextureArray(const CTextureArray& other);
	CTextureArray(CTextureArray&& other);
	CTextureArray& operator= (const CTextureArray& other);
	CTextureArray& operator= (CTextureArray&& other);

public:

	ukl::UKLResult initialise(ID3D11Device* device, WCHAR* filename1, WCHAR* filename2=nullptr);
	void terminate();

	ID3D11ShaderResourceView** getTextureArray();

private:

	ID3D11ShaderResourceView** m_textures;
	unsigned int m_size;
};

#endif // !TEXTURE_ARRAY_H