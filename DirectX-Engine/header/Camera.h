//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef CAMERA_H
#define CAMERA_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

class CCamera
{
public:
	CCamera();
	~CCamera();
private:
	// disallow copying and moving
	CCamera(const CCamera& other);
	CCamera(CCamera&& other);
	CCamera& operator= (const CCamera& other);
	CCamera& operator= (CCamera&& other);

public:
	void setPosition(float x, float y, float z);
	void setRotation(float x, float y, float z);

	D3DXVECTOR3 getPosition() const;
	D3DXVECTOR3 getRotation() const;

	void render();
	void getViewMatrix(D3DXMATRIX& viewMatrix) const;

	void renderReflection(float height);
	D3DXMATRIX getReflectionViewMatrix();

protected:
	D3DXVECTOR3 m_pos;
	float m_rotX, m_rotY, m_rotZ;
	D3DXMATRIX m_viewMatrix, m_reflectionViewMatrix;

};

#endif //! CAMERA_H