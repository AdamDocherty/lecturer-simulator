//
// Author: Adam Docherty
// Date:   2013-11-28
//

#ifndef TOON_SHADER_H
#define TOON_SHADER_H

//_____________________________________________________________________________
//
// Shader for rendering things to appear as if they are a cartoon.

#include "DirectXPCH.h"

#include <d3dx11async.h>
#include <fstream>

class CToonShader
{
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct IntensityCutoffType
	{
		D3DXVECTOR4 cutoffValues;
	};

	struct LightBufferType
	{
		D3DXVECTOR4 ambientColor;
		D3DXVECTOR4 diffuseColor;
		D3DXVECTOR3 lightDirection;
		float padding;
	};
public:
	CToonShader();
	~CToonShader();
private:
	// disallow copying and moving
	CToonShader(const CToonShader& other);
	CToonShader(CToonShader&& other);
	CToonShader& operator= (const CToonShader& other);
	CToonShader& operator= (CToonShader&& other);

public:
	ukl::UKLResult initialise(ID3D11Device* device, HWND hwnd);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
		D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambientColor,
		D3DXVECTOR4 diffuseColor, D3DXVECTOR3 cameraPosition, D3DXVECTOR4 cutoffValues);

private:
	ukl::UKLResult initialiseShader(ID3D11Device* device, HWND hwnd, WCHAR* vsFilename, WCHAR* psFilename);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFilename);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
		D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR3 lightDirection, 
		D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor, D3DXVECTOR3 cameraPosition, D3DXVECTOR4 cutoffValues);
	void renderShader(ID3D11DeviceContext* deviceContext, int indexCount);

private:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;

	ID3D11InputLayout* m_layout;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* m_matrixBuffer;

	ID3D11Buffer* m_lightBuffer;
	ID3D11Buffer* m_intensityCutoffBuffer;
};

#endif //! TOON_SHADER_H