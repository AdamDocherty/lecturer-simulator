//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef LIGHT_H
#define LIGHT_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

class CLight
{
public:
	CLight();
	~CLight();
private:
	// disallow copying and moving
	CLight(const CLight& other);
	CLight(CLight&& other);
	CLight& operator= (const CLight& other);
	CLight& operator= (CLight&& other);

public:
	void setDiffuseColour(float r, float g, float b, float a);
	void setDirection(float x, float y, float z);

	D3DXVECTOR4 getDiffuseColour();
	D3DXVECTOR3 getDirection();

private:
	D3DXVECTOR4 m_diffuseColour;
	D3DXVECTOR3 m_direction;
};

#endif //! LIGHT_H