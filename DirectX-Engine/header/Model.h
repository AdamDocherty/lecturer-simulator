//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef MODEL_H
#define MODEL_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "TextureArray.h"

class CModel
{
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};

	struct ModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
		float tx, ty, tz;
        float bx, by, bz;
	};

	struct TempVertexType
    {
        float x, y, z;
        float tu, tv;
        float nx, ny, nz;
    };

	struct VectorType
    {
        float x, y, z;
    };

public:
	CModel();
	~CModel();
private:
	// disallow copying and moving
	CModel(const CModel& other);
	CModel(CModel&& other);
	CModel& operator= (const CModel& other);
	CModel& operator= (CModel&& other);

public:
	ukl::UKLResult initialise(ID3D11Device* device, char* modelFilename, WCHAR* textureFilename0, WCHAR *textureFilename1=nullptr);
	void terminate();
	void render(ID3D11DeviceContext* deviceContext, bool controlPoint=false);

	int getIndexCount();
	D3DXMATRIX* getMatrix();

	ID3D11ShaderResourceView **getTextureArray();

private:
	ukl::UKLResult initialiseBuffers(ID3D11Device* device);
	void terminateBuffers();
	void renderBuffers(ID3D11DeviceContext *deviceContext, bool controlPoint);

	ukl::UKLResult loadTexture(ID3D11Device *device, WCHAR *textureFilename0, WCHAR *textureFilename1=nullptr);
	void releaseTexture();

	ukl::UKLResult loadModelRasterTek(char *filename);
	ukl::UKLResult loadModelOBJ(char *filename);
	void calculateModelVectors();
	void calculateTangentBinormal(TempVertexType vertex1, TempVertexType vertex2, TempVertexType vertex3,
                      VectorType& tangent, VectorType& binormal);
	void calculateNormal(VectorType tangent, VectorType binormal, VectorType& normal);
	void releaseModel();

private:
	D3DXMATRIX m_matrix;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	CTextureArray* m_textureArray;
	ModelType* m_model;

};

#endif //! MODEL_H