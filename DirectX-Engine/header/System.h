//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef SYSTEM_H
#define SYSTEM_H

//______________________________________________________________________________
//
// A class that encapsulates the entire application that will be called from
// within the WinMain function. Based on the SystemClass provided by Paul
// Robertson and RasterTek.

#include "DirectXPCH.h"

#include "Input.h"
#include "Graphics.h"
#include "Fps.h"
#include "Cpu.h"
#include "Timer.h"

#include "FirstPersonCamera.h"

class CSystem
{
public:
	CSystem();
	~CSystem();
private:
	// disallow copying and moving
	CSystem(const CSystem& other);
	CSystem(CSystem&& other);
	CSystem& operator= (const CSystem& other);
	CSystem& operator= (CSystem&& other);

public:
	ukl::UKLResult	initialise();
	void		terminate();
	ukl::UKLResult	run();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
	ukl::UKLResult	frame();

	ukl::UKLResult   handleInput();
	ukl::UKLResult	update();
	ukl::UKLResult	render();

	ukl::UKLResult	initialiseWindows(int& screenWidth, int& screenHeight);
	void		terminateWindows();

private:
	LPCWSTR		m_applicationName;
	HINSTANCE	m_hInstance;
	HWND		m_hwnd;

private: 
	static const unsigned WINDOWED_SCREEN_WIDTH = 800;
	static const unsigned WINDOWED_SCREEN_HEIGHT = 600;

	static const float CAMERA_SPEED;
	static const float CAMERA_SLOW_SPEED;

	bool		m_running;
	bool		m_paused;
	float		m_timeElapsed;

	CInput	   *m_input;
	CGraphics  *m_graphics;
	CFps	   *m_fps;
	CCpu       *m_cpu;
	CTimer     *m_timer; 

	CFirstPersonCamera	   *m_camera;

	int 		m_screenCentre[2];
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

static CSystem *g_applicationHandle;

#endif //! SYSTEM_H