//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef FONT_SHADER_H
#define FONT_SHADER_H

//______________________________________________________________________________
//

#include "UKL.h"

#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>

class CFontShader
{
	struct ConstantBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct PixelBufferType
	{
		D3DXVECTOR4 pixelColor;
	};

public:
	CFontShader();
	~CFontShader();
private:
	// disallow copying and moving
	CFontShader(const CFontShader& other);
	CFontShader(CFontShader&& other);
	CFontShader& operator= (const CFontShader& other);
	CFontShader& operator= (CFontShader&& other);

public:
	ukl::UKLResult initialise(ID3D11Device*, HWND);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR4);

private:
	ukl::UKLResult initialiseShader(ID3D11Device*, HWND, WCHAR*, WCHAR*);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR4);
	void renderShader(ID3D11DeviceContext*, int);

private:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_constantBuffer;
	ID3D11SamplerState* m_sampleState;

	ID3D11Buffer* m_pixelBuffer;
};

#endif //! FONT_SHADER_H