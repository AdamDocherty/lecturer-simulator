#ifndef TESSELLATION_SHADER_H
#define TESSELLATION_SHADER_H

#include "DirectXPCH.h"
#include <d3dx11async.h>
#include <fstream>

class CTessellationShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct TessellationBufferType
	{
		float tessellationAmount;
		D3DXVECTOR3 padding;
	};

public:
	CTessellationShader();
	~CTessellationShader();

	ukl::UKLResult initialise(ID3D11Device*, HWND);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, float);

private:
	ukl::UKLResult initialiseShader(ID3D11Device*, HWND, WCHAR*, WCHAR*, WCHAR*, WCHAR*);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, float);
	void renderShader(ID3D11DeviceContext*, int);

private:
	ID3D11VertexShader* m_vertexShader;

	ID3D11HullShader* m_hullShader;
	ID3D11DomainShader* m_domainShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;

	ID3D11Buffer* m_tessellationBuffer;
};

#endif

