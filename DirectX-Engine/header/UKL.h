//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef UKL_H
#define UKL_H

//______________________________________________________________________________
//
// Header file containing useful macros for use throughout the project.
//
// All macros are prefixed with UKL_ 
// All enumerations are prefixed with UKLE_


#include <string>
#include <iostream>
#include <sstream>

#include <Windows.h>

namespace ukl
{

	enum UKLResult
	{
		UKLE_RESULT_NO_ERROR = 0,
		UKLE_RESULT_UNKNOWN_ERROR,
		UKLE_RESULT_FILE_NOT_FOUND,
		UKLE_RESULT_OUT_OF_BOUNDS,
		UKLE_RESULT_BAD_ALLOC,

		UKLE_RESULT_COUNT
	};

	static UKLResult lastResult = UKLE_RESULT_NO_ERROR;
	extern std::wstring lastResultInfo; // declared in DirectXPCH.cpp
	static void outputLastError()
	{
		if (lastResult != UKLE_RESULT_NO_ERROR)
		{
			std::wostringstream title_;
			title_ << "Error Code: " << lastResult;
			MessageBox(NULL, lastResultInfo.c_str(), 
				title_.str().c_str(), MB_OK | MB_ICONSTOP);
		}
	}
}

#define UKL_TRY(expr)										\
	ukl::lastResult = (expr);								\
	if (ukl::lastResult != ukl::UKLE_RESULT_NO_ERROR)		\
		{ return ukl::lastResult; }

#define UKL_TRY_REPORT(expr)								\
	ukl::lastResult = (expr);								\
	if (ukl::lastResult != ukl::UKLE_RESULT_NO_ERROR)		\
	{														\
		ukl::outputLastError();								\
		return ukl::lastResult;								\
	}	

#define UKL_PRINTF(s)										\
{															\
	std::wostringstream os_;								\
	os_ << s;												\
	OutputDebugStringW(os_.str().c_str());					\
}

#define UKL_ERROR(resultType, s)							\
{															\
	std::wostringstream errorMessage_;						\
	errorMessage_ << "File: " << __FILE__ <<				\
		"\nLine: " << __LINE__ << "\n\n" << s;				\
	ukl::lastResultInfo = errorMessage_.str();				\
	return ukl::resultType;									\
}

#define UKL_DELETE(obj)										\
	if(obj) { delete obj; obj = 0; }

#define UKL_TERMINATE_AND_DELETE(obj)						\
	if(obj) { obj->terminate(); delete obj; obj = 0; }

#endif //! UKL_H