//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef FPS_H
#define FPS_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include <MMSystem.h>

class CFps
{
public:
	CFps();
	~CFps();
private:
	// disallow copying and moving
	CFps(const CFps& other);
	CFps(CFps&& other);
	CFps& operator= (const CFps& other);
	CFps& operator= (CFps&& other);

public:
	ukl::UKLResult initialise();
	void frame();
	int getFps();

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};

#endif //! FPS_H