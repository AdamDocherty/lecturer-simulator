//
// Author: Adam Docherty
// Date:   30-10-2013
//

#ifndef D3D_H
#define D3D_H

//______________________________________________________________________________
//

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dx10.lib")
#pragma comment(lib, "winmm.lib")

#include "DirectXPCH.h"

class CD3D
{
public:
	CD3D();
	~CD3D();
private:
	// disallow copying and moving
	CD3D(const CD3D& other);
	CD3D(CD3D&& other);
	CD3D& operator= (const CD3D& other);
	CD3D& operator= (CD3D&& other);

public:
	ukl::UKLResult initialise(int, int, bool, HWND, bool, float, float);
	void terminate();

	void beginScene(float r, float g, float b, float a);
	void endScene();

	ID3D11Device* getDevice();
	ID3D11DeviceContext* getDeviceContext();

	void getProjectionMatrix(D3DXMATRIX& projectionMatrix);
	void getWorldMatrix(D3DXMATRIX& worldMatrix);
	void getOrthoMatrix(D3DXMATRIX& orthoMatrix);

	void getVideoCardInfo(char*, int&);

	void turnZBufferOn();
	void turnZBufferOff();

	void turnAlphaBlendingOn();
	void turnAlphaBlendingOff();

	void turnWireframeOn();
	void turnWireframeOff();
	bool isWireframeEnabled() { return m_wireframe_enabled; }

	void turnFrontfaceCullingOn();
	void turnFrontfaceCullingOff();

	ID3D11DepthStencilView* getDepthStencilView();
	void setBackBufferRenderTarget();
	void resetViewport();

private:
	bool m_vsync_enabled;
	bool m_wireframe_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	ID3D11RenderTargetView* m_renderTargetView;
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilView* m_depthStencilView;
	ID3D11RasterizerState* m_rasterState;
	ID3D11RasterizerState* m_frontCullingRasterState;
	ID3D11RasterizerState* m_wireframeRasterState;
	D3DXMATRIX m_projectionMatrix;
	D3DXMATRIX m_worldMatrix;
	D3DXMATRIX m_orthoMatrix;
	D3D11_VIEWPORT m_viewport;

	ID3D11DepthStencilState* m_depthStencilState;
	ID3D11DepthStencilState* m_depthDisabledStencilState;

	ID3D11BlendState* m_alphaEnableBlendingState;
	ID3D11BlendState* m_alphaDisableBlendingState;
};

#endif //! D3D_H