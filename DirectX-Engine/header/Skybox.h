//
// Author: Adam Docherty
// Date: 2013-11-26
//

#ifndef SKYBOX_H
#define SKYBOX_H

//_____________________________________________________________________________
//

#include "DirectXPCH.h"

#include "Model.h"
#include "Camera.h"
#include "TextureShader.h"

class CSkybox
{
public:
	CSkybox();
	~CSkybox();
private:
	// disallow copying and moving
	CSkybox(const CSkybox& other);
	CSkybox(CSkybox&& other);
	CSkybox& operator= (const CSkybox& other);
	CSkybox& operator= (CSkybox&& other);

public:
	ukl::UKLResult initialise(ID3D11Device* device);
	void terminate();
	
	void setCamera(CCamera *camera);

	void render();
	void renderReflection(float height);

	CModel *getModel();
protected:
private:
	CModel     *m_model;	// Skybox model
	CCamera    *m_camera;	// The camera the skybox is currently attached to
};

#endif //! SKYBOX_H