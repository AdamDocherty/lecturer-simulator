//
// Author: Adam Docherty
// Date:   2013-11-13
//

#ifndef RENDER_TEXTURE_H_
#define RENDER_TEXTURE_H_

//_____________________________________________________________________________
//

#include "DirectXPCH.h"

class CRenderTexture
{
public:
	CRenderTexture();
	~CRenderTexture();
private:
	// disallow copying and moving
	CRenderTexture(const CRenderTexture& other);
	CRenderTexture(CRenderTexture&& other);
	CRenderTexture& operator= (const CRenderTexture& other);
	CRenderTexture& operator= (CRenderTexture&& other);

public:

	ukl::UKLResult initialise(ID3D11Device* device, int textureWidth, int textureHeight);
	void terminate();

	void setRenderTarget(ID3D11DeviceContext* deviceContext, ID3D11DepthStencilView* depthStencilView);
	void clearRenderTarget(ID3D11DeviceContext* deviceContext, ID3D11DepthStencilView* depthStencilView, 
					   float red, float green, float blue, float alpha);
	ID3D11ShaderResourceView* getShaderResourceView();

private:
	ID3D11Texture2D* m_renderTargetTexture;
	ID3D11RenderTargetView* m_renderTargetView;
	ID3D11ShaderResourceView* m_shaderResourceView;
};

#endif
