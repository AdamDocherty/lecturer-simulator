//
// Author: Adam Docherty
// Date:   03-11-2013
//

#ifndef FIRST_PERSON_CAMERA_H
#define FIRST_PERSON_CAMERA_H

//______________________________________________________________________________
//

#include "DirectXPCH.h"

#include "Camera.h"

class CFirstPersonCamera : public CCamera
{
public:
	CFirstPersonCamera();
	~CFirstPersonCamera();

private:
	// disallow copying and moving
	CFirstPersonCamera(const CFirstPersonCamera& other);
	CFirstPersonCamera(CFirstPersonCamera&& other);
	CFirstPersonCamera& operator= (const CFirstPersonCamera& other);
	CFirstPersonCamera& operator= (CFirstPersonCamera&& other);

public:
	// move relative to the world
	void move(float x, float y, float z);
	// move relative to the direction the camera is facing
	void moveForward(float x, float y, float z);

	void rotate(float x, float y, float z);
};

#endif //! FIRST_PERSON_CAMERA_H