#include "OutlineShader.h"

using namespace ukl;

COutlineShader::COutlineShader()
	: m_vertexShader(NULL)
	, m_pixelShader(NULL)
	, m_layout(NULL)
	, m_matrixBuffer(NULL)
	, m_lineThicknessBuffer(NULL)
	, m_lineColourBuffer(NULL)
{
}

COutlineShader::~COutlineShader()
{
}

UKLResult COutlineShader::initialise(ID3D11Device* device, HWND hwnd)
{
	UKL_TRY(initialiseShader(device, hwnd, L"shaders/toonPass2.vert", L"shaders/toonPass2.frag"));
	return UKLE_RESULT_NO_ERROR;
}

void COutlineShader::terminate()
{
	terminateShader();
}

UKLResult COutlineShader::render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, 
	D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXVECTOR4 colour, float thickness)
{
	UKL_TRY(setShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix, colour, thickness));
	renderShader(deviceContext, indexCount);

	return UKLE_RESULT_NO_ERROR;
}

UKLResult COutlineShader::initialiseShader(ID3D11Device* device, HWND hwnd, WCHAR* vsFilename, WCHAR* psFilename)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* pixelShaderBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC lineThicknessBufferDesc;
	D3D11_BUFFER_DESC lineColourBufferDesc;

	// Initialize the pointers this function will use to null.
	errorMessage = 0;
	vertexShaderBuffer = 0;
	pixelShaderBuffer = 0;

	// Compile the vertex shader code.
	result = D3DX11CompileFromFile(vsFilename, NULL, NULL, "OutlineVertexShader", "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, 
		&vertexShaderBuffer, &errorMessage, NULL);
	if(FAILED(result))
	{
		// If the shader failed to compile it should have written something to the error message.
		if(errorMessage)
		{
			outputShaderErrorMessage(errorMessage, hwnd, vsFilename);
			UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "Error in " << vsFilename << ". See shader-error.txt for details");
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else
		{
			UKL_ERROR(UKLE_RESULT_FILE_NOT_FOUND, "Could not find file " << vsFilename);
		}
	}

	// Compile the pixel shader code.
	result = D3DX11CompileFromFile(psFilename, NULL, NULL, "OutlinePixelShader", "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, 
		&pixelShaderBuffer, &errorMessage, NULL);
	if(FAILED(result))
	{
		// If the shader failed to compile it should have written something to the error message.
		if(errorMessage)
		{
			outputShaderErrorMessage(errorMessage, hwnd, psFilename);
			UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "Error in " << psFilename << ". See shader-error.txt for details");
		}
		// If there was  nothing in the error message then it simply could not find the file itself.
		else
		{
			UKL_ERROR(UKLE_RESULT_FILE_NOT_FOUND, "Could not find file " <<psFilename);
		}
	}

	// Create the vertex shader from the buffer.
	result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &m_vertexShader);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Create the pixel shader from the buffer.
	result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &m_pixelShader);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Now setup the layout of the data that goes into the shader.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;
	
	// Get a count of the elements in the layout.
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Create the vertex input layout.
	result = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), 
		vertexShaderBuffer->GetBufferSize(), &m_layout);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	lineThicknessBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lineThicknessBufferDesc.ByteWidth = sizeof(LineThicknessType);
	lineThicknessBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lineThicknessBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lineThicknessBufferDesc.MiscFlags = 0;
	lineThicknessBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&lineThicknessBufferDesc, NULL, &m_lineThicknessBuffer);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	lineColourBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lineColourBufferDesc.ByteWidth = sizeof(LineColourType);
	lineColourBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lineColourBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lineColourBufferDesc.MiscFlags = 0;
	lineColourBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	result = device->CreateBuffer(&lineColourBufferDesc, NULL, &m_lineColourBuffer);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	return UKLE_RESULT_NO_ERROR;
}

void COutlineShader::terminateShader()
{
	if(m_lineColourBuffer)
	{
		m_lineColourBuffer->Release();
		m_lineColourBuffer =0;
	}

	if(m_lineThicknessBuffer)
	{
		m_lineThicknessBuffer->Release();
		m_lineThicknessBuffer =0;
	}

	if(m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer =0;
	}

	if(m_layout)
	{
		m_layout->Release();
		m_layout =0;
	}

	if(m_pixelShader)
	{
		m_pixelShader->Release();
		m_pixelShader =0;
	}

	if(m_vertexShader)
	{
		m_vertexShader->Release();
		m_vertexShader =0;
	}
}

void COutlineShader::outputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFilename)
{
	char* compileErrors;
	unsigned long bufferSize, i;
	std::ofstream fout;


	// Get a pointer to the error message text buffer.
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for(i=0; i<bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	// Close the file.
	fout.close();

	// Release the error message.
	errorMessage->Release();
	errorMessage = 0;
}

UKLResult COutlineShader::setShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, 
	D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXVECTOR4 colour, float thickness)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	LineThicknessType* dataPtr2;
	LineColourType* dataPtr3;
	unsigned int bufferNumber;

	// Transpose the matrices to prepare them for the shader.
	D3DXMatrixTranspose(&worldMatrix, &worldMatrix);
	D3DXMatrixTranspose(&viewMatrix, &viewMatrix);
	D3DXMatrixTranspose(&projectionMatrix, &projectionMatrix);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = worldMatrix;
	dataPtr->view = viewMatrix;
	dataPtr->projection = projectionMatrix;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_matrixBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Finanly set the constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_lineThicknessBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr2 = (LineThicknessType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr2->thickness = thickness;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_lineThicknessBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 1;

	// Finanly set the constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_lineThicknessBuffer);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_lineColourBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "");
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr3 = (LineColourType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr3->colour = colour;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_lineColourBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Finanly set the constant buffer in the vertex shader with the updated values.
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_lineColourBuffer);

	return UKLE_RESULT_NO_ERROR;
}

void COutlineShader::renderShader(ID3D11DeviceContext* deviceContext, int indexCount)
{
	// Set the vertex input layout.
	deviceContext->IASetInputLayout(m_layout);

	// Set the vertex and pixel shaders that will be used to render this triangle.
	deviceContext->VSSetShader(m_vertexShader, NULL, 0);
	deviceContext->PSSetShader(m_pixelShader, NULL, 0);

	// Render the triangle.
	deviceContext->DrawIndexed(indexCount, 0, 0);
}