#ifndef OUTLINE_SHADER_H
#define OUTLINE_SHADER_H

#include "DirectXPCH.h"

#include <d3dx11async.h>
#include <fstream>

class COutlineShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct LineThicknessType
	{
		float thickness;
		D3DXVECTOR3 padding;
	};

	struct LineColourType
	{
		D3DXVECTOR4 colour;
	};
public:
	COutlineShader();
	~COutlineShader();

public:
	ukl::UKLResult initialise(ID3D11Device* device, HWND hwnd);
	void terminate();
	ukl::UKLResult render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, 
		D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXVECTOR4 colour, float thickness);

private:
	ukl::UKLResult initialiseShader(ID3D11Device* device, HWND hwnd, WCHAR* vsFilename, WCHAR* psFilename);
	void terminateShader();
	void outputShaderErrorMessage(ID3D10Blob*, HWND hwnd, WCHAR*);

	ukl::UKLResult setShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, 
		D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXVECTOR4 colour, float thickness);
	void renderShader(ID3D11DeviceContext* deviceContext, int indexCount);

private:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
	ID3D11Buffer* m_lineThicknessBuffer;
	ID3D11Buffer* m_lineColourBuffer;
};

#endif//! OUTLINE_SHADER_H