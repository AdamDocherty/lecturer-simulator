Texture2D shaderTexture;
SamplerState SampleType;

cbuffer LineColourBuffer
{
	float4 lineColor;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
};

float4 OutlinePixelShader(PixelInputType input) : SV_TARGET
{
	return lineColor;
}

