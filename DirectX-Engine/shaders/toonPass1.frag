Texture2D shaderTexture;
SamplerState SampleType;

cbuffer LightBuffer
{
	float4 ambientColor;
    float4 diffuseColor;
    float3 lightDirection;
	float buffer;
};

cbuffer IntensityCutoffBuffer
{
	float4 cutoffValues;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

float4 LightPixelShader(PixelInputType input) : SV_TARGET
{
	float3 lightDir = -lightDirection;
	
    // Calculate diffuse light amount
    float intensity = saturate(dot(input.normal, lightDir));
 
    // Calculate what would normally be the final color, including texturing and diffuse lighting
    float4 color = shaderTexture.Sample(SampleType, input.tex) * diffuseColor;
    color.a = 1;
 
	//color = round(intensity * 5) / 5 * color;
	
	float roundValue = 0.125f;
	
	color[0] = round(color[0] / roundValue) * roundValue;
	color[1] = round(color[1] / roundValue) * roundValue;
	color[2] = round(color[2] / roundValue) * roundValue;
	
	color = saturate(color);
 
    // Discretize the intensity, based on a few cutoff points
    if (intensity > cutoffValues[0])
        color = float4(1.0,1,1,1.0) * color;
    else if (intensity > cutoffValues[1])
        color = float4(0.7,0.7,0.7,1.0) * color;
    else if (intensity > cutoffValues[2])
        color = float4(0.35,0.35,0.35,1.0) * color;
    else
        color = float4(0.1,0.1,0.1,1.0) * color;
 
    return color;
}

