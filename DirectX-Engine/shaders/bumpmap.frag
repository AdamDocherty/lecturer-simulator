Texture2D shaderTextures[2];
SamplerState SampleType;

cbuffer LightBuffer
{
	float4 ambientColor;
	float4 diffuseColor;
	float3 lightDirection;
};

struct PixelInputType
{   

	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
        float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
		
};

float4 BumpMapPixelShader(PixelInputType input) : SV_TARGET
{
	float4 textureColor;
	float4 bumpMap;
	float3 bumpNormal;
	float3 lightDir;
	float  lightIntensity;
	float4 color;

	textureColor = shaderTextures[0].Sample(SampleType, input.tex);
	bumpMap = shaderTextures[1].Sample(SampleType, input.tex);

	color = ambientColor;

	// Expand the range of the normal values from (0, +1) to (-1, +1)
	// the sampled value that is presented to us in the 0.0 to +1.0 texture
	// range which only covers half the range we need for the bump map
	// normal calculations
	bumpMap = (bumpMap * 2.0f) - 1.0f;

	// Calculate the normal from the data in the bump map.
	bumpNormal = input.normal + bumpMap.x * input.tangent + bumpMap.y * input.binormal;

	bumpNormal = normalize(bumpNormal);

	lightDir = -lightDirection;

	// Calculate light on this pixel based on the bump map normal value
	lightIntensity = saturate(dot(bumpNormal, lightDir));

	color += saturate(diffuseColor * lightIntensity);

	color = color * textureColor;

	return color;
};
