//
// Author: Adam Docherty
// Date:   30-10-2013
//______________________________________________________________________________
//
// A class that encapsulates the entire application that will be called from
// within the WinMain function. Based on the SystemClass provided by Paul
// Robertson and RasterTek.

#include "System.h"

const float CSystem::CAMERA_SPEED = 50.f;
const float CSystem::CAMERA_SLOW_SPEED = 5.0f;

using namespace ukl;

CSystem::CSystem()
	: m_paused(false)
	, m_input(NULL)
	, m_graphics(NULL)
	, m_fps(NULL)
	, m_cpu(NULL)
	, m_timer(NULL)
	, m_running(true)
{ 
}

CSystem::~CSystem()
{
}

UKLResult CSystem::initialise()
{
	int screenWidth = 0, screenHeight = 0;

	initialiseWindows(screenWidth, screenHeight);

	m_screenCentre[0] = screenWidth / 2;
	m_screenCentre[1] = screenHeight / 2;

	m_input = new CInput;
	UKL_TRY(m_input->initialise(m_hwnd));

	m_camera = new CFirstPersonCamera;

	// Set the initial position of the camera
	m_camera->setPosition(0.0f, 0.0f, -10.0f);
	m_camera->setRotation(0.0f, 0.0f, 0.0f);
	m_camera->render();

	// Create a new graphics object. This object will handle rendering
	// all the graphics for this application.
	m_graphics = new CGraphics;
	UKL_TRY(m_graphics->initialise(m_camera, screenWidth, screenHeight, m_hwnd));

	m_fps = new CFps;
	UKL_TRY(m_fps->initialise());

	m_cpu = new CCpu;
	UKL_TRY(m_cpu->initialise());

	m_timer = new CTimer;
	UKL_TRY(m_timer->initialise());

	m_input->setMousePos(m_screenCentre[0], m_screenCentre[1]);

	return UKLE_RESULT_NO_ERROR;
}

void CSystem::terminate()
{
	UKL_DELETE(m_input);
	UKL_TERMINATE_AND_DELETE(m_cpu);
	UKL_DELETE(m_fps);
	UKL_DELETE(m_camera);
	UKL_TERMINATE_AND_DELETE(m_graphics);

	terminateWindows();
}

UKLResult CSystem::run()
{
	MSG msg;

	ZeroMemory(&msg, sizeof(MSG));
	while(m_running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				return UKLE_RESULT_NO_ERROR;
			}
		}
		else
		{
			UKL_TRY(frame());
		}
	}

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CSystem::frame()
{
	if (m_hwnd != GetActiveWindow()) return UKLE_RESULT_NO_ERROR;

	UKL_TRY(handleInput());

	if (m_paused) return UKLE_RESULT_NO_ERROR;

	m_timer->frame();
	m_fps->frame();
	m_cpu->frame();

	m_timeElapsed = m_timer->getTime() / 1000; // convert to ms

	UKL_TRY(update());
	UKL_TRY(render());

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CSystem::handleInput()
{
	if(m_input->isKeyDown(VK_ESCAPE))
	{
		m_running = false;
		return UKLE_RESULT_NO_ERROR;
	}

	if(m_input->isKeyDown(VK_SHIFT) &&
		m_input->isKeyDown('C'))
	{
		m_running = false;
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, "You have hit the crash button " << 1 << L" times. Baka!");
	}

	if(m_input->isKeyPressed('P'))
	{
		if(m_paused)
		{
			// reset the timer and mouse position
			m_timer->frame();
			m_input->setMousePos(m_screenCentre[0], m_screenCentre[1]);
		}
		m_paused = !m_paused;
	}

	if(m_input->isKeyPressed('I'))
	{
		if(m_graphics->getD3D()->isWireframeEnabled())
		{
			m_graphics->getD3D()->turnWireframeOff();
		}
		else
		{
			m_graphics->getD3D()->turnWireframeOn();
		}
	}

	if(!m_paused)
	{
		// Keys for rotating the camera
		int x = 0, y = 0;

		m_input->getMousePos(&x, &y);
		m_camera->rotate(((float) y - m_screenCentre[1]) * 0.2f, ((float) x - m_screenCentre[0]) * 0.2f, 0);
		m_input->setMousePos(m_screenCentre[0], m_screenCentre[1]);

		float cameraSpeed = CAMERA_SPEED;

		if(m_input->isKeyDown(VK_CONTROL))
		{
			cameraSpeed = CAMERA_SLOW_SPEED;
		}
		// Keys for moving the camera
		if( /* Left and right mouse buttons */
			(m_input->isButtonDown(MouseButtons::LEFT) && 
			m_input->isButtonDown(MouseButtons::RIGHT)) ||
			/* Or the 'W' Key */
			m_input->isKeyDown('W'))
		{
			m_camera->moveForward(0.0f, 0.0f, cameraSpeed * m_timeElapsed);
		}

		if(m_input->isKeyDown('S'))	{
			m_camera->moveForward(0.0f, 0.0f, -cameraSpeed * m_timeElapsed);
		}
		if(m_input->isKeyDown('A'))	{
			m_camera->moveForward(-cameraSpeed * m_timeElapsed, 0.0f, 0.0f);
		}
		if(m_input->isKeyDown('D'))	{
			m_camera->moveForward(cameraSpeed * m_timeElapsed, 0.0f, 0.0f);
		}
		if(m_input->isKeyDown(VK_SHIFT)) {
			m_camera->move(0.0f, -cameraSpeed * m_timeElapsed, 0.0f);
		}
		if(m_input->isKeyDown(VK_SPACE)) {
			m_camera->move(0.0f, cameraSpeed * m_timeElapsed, 0.0f);
		}

		// Reset position of the camera
		if(m_input->isKeyDown('R'))
		{
			m_camera->setPosition(0.0f, 0.0f, -10.0f);
			m_camera->setRotation(0.0f, 0.0f, 0.0f);
		}
	}

	m_input->refresh();

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CSystem::update()
{
	return UKLE_RESULT_NO_ERROR;
}

UKLResult CSystem::render()
{
	UKL_TRY(m_graphics->frame(m_camera, m_fps->getFps(), m_cpu->getCpuPercentage(), m_timeElapsed));
	return UKLE_RESULT_NO_ERROR;
}

LRESULT CALLBACK CSystem::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch(umsg)
	{
		// Check if a key has been pressed on the keyboard.
	case WM_KEYDOWN:
		{
			// Only update input if the previous key state was up
			if (!(lparam & 0x40000000))
				// If a key is pressed send it to the input object so it can record that state.
				m_input->keyDown((unsigned int)wparam);
			return 0;
		}
		break;

		// Check if a key has been released on the keyboard.
	case WM_KEYUP:
		{
			// If a key is released then send it to the input object so it can unset the state for that key.
			m_input->keyUp((unsigned int)wparam);
			return 0;
		}
		break;

	case WM_LBUTTONDOWN:
		{
			m_input->buttonDown(MouseButtons::LEFT);
		}
		break;

	case WM_LBUTTONUP:
		{
			m_input->buttonUp(MouseButtons::LEFT);
		}
		break;

	case WM_RBUTTONDOWN:
		{
			m_input->buttonDown(MouseButtons::RIGHT);
		}
		break;

	case WM_RBUTTONUP:
		{
			m_input->buttonUp(MouseButtons::RIGHT);
		}
		break;

	case WM_MBUTTONDOWN:
		{
			m_input->buttonDown(MouseButtons::MIDDLE);
		}
		break;

	case WM_MBUTTONUP:
		{
			m_input->buttonUp(MouseButtons::MIDDLE);
		}
		break;
	
	case WM_SETFOCUS:
		{
			if(m_input) m_input->setMousePos(m_screenCentre[0], m_screenCentre[1]);
		}
		break ;

		// Any other messages send to the default message handler as our application won't make use of them.
	default:
		{
			return DefWindowProc(hwnd, umsg, wparam, lparam);
		}
	}
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

UKLResult CSystem::initialiseWindows(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;


	// Get an external pointer to this object.
	g_applicationHandle = this;

	// Get the instance of this application.
	m_hInstance = GetModuleHandle(NULL);

	// Give the application a name.
	m_applicationName = L"Engine";

	// Setup the windows class with default settings.
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if(FULL_SCREEN)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		// If windowed then set it to 800x600 resolution.
		screenWidth  = WINDOWED_SCREEN_WIDTH;
		screenHeight = WINDOWED_SCREEN_HEIGHT;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX, posY, screenWidth, screenHeight, NULL, NULL, m_hInstance, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);

	// Hide the mouse cursor.
	ShowCursor(false);

	return UKLE_RESULT_NO_ERROR;
}

void CSystem::terminateWindows()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(FULL_SCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hInstance);
	m_hInstance = NULL;

	// Release the pointer to this class.
	g_applicationHandle = NULL;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		// Check if the window is being destroyed.
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
	case WM_CLOSE:
		{
			PostQuitMessage(0);		
			return 0;
		}

		// All other messages pass to the message handler in the system class.
	default:
		{
			return g_applicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		}
	}
}
