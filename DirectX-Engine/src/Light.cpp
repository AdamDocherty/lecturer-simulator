#include "Light.h"

using namespace ukl;

CLight::CLight()
{
}

CLight::~CLight()
{
}


void CLight::setDiffuseColour(float red, float green, float blue, float alpha)
{
	m_diffuseColour = D3DXVECTOR4(red, green, blue, alpha);
	return;
}


void CLight::setDirection(float x, float y, float z)
{
	m_direction = D3DXVECTOR3(x, y, z);
	return;
}


D3DXVECTOR4 CLight::getDiffuseColour()
{
	return m_diffuseColour;
}


D3DXVECTOR3 CLight::getDirection()
{
	return m_direction;
}