//
// Author: Adam Docherty
// Date:   30-10-2013
//______________________________________________________________________________
//

#include "Input.h"

using namespace ukl;

CInput::CInput()
{
}

CInput::~CInput()
{
}

UKLResult CInput::initialise(HWND hwnd)
{
	m_hwnd = hwnd;

	int i;

	for (i = 0; i < NUM_KEYS; ++i)
	{
		m_keys[i] = false;
	}
	for (i = 0; i < MouseButtons::NUM_MOUSE_BUTTONS; ++i)
	{
		m_mouseButtons[i] = 0;
	}

	refresh();

	return UKLE_RESULT_NO_ERROR;
}

void CInput::refresh()
{
	int i;

	for (i=0;i<NUM_KEYS;++i)
	{
		m_keyPressed[i] = false;
		m_keyReleased[i] = false;
	}

	for (i=0;i<MouseButtons::NUM_MOUSE_BUTTONS;++i)
	{
		m_mouseButtonPressed[i] = false;
		m_mouseButtonReleased[i] = false;
	}
}

void CInput::keyDown(unsigned int input)
{
	// If a key is pressed then save that state in the key array.
	m_keys[input] = true;
	m_keyPressed[input] = true;
}

void CInput::keyUp(unsigned int input)
{
	// If a key is released then clear that state in the key array.
	m_keys[input] = false;
	m_keyReleased[input] = true;
}

void CInput::buttonDown(MouseButton button)
{
	m_mouseButtons[button] = true;
	m_mouseButtonPressed[button] = true;
}

void CInput::buttonUp(MouseButton button)
{
	m_mouseButtons[button] = false;
	m_mouseButtonReleased[button] = true;
}

bool CInput::isKeyDown(unsigned int key)
{
	// Return what state the key is in (pressed/not pressed).
	return m_keys[key];
}

bool CInput::isKeyPressed(unsigned int key)
{
	return m_keyPressed[key];
}

bool CInput::isKeyReleased(unsigned int key)
{
	return m_keyReleased[key];
}

bool CInput::isButtonDown(MouseButton button)
{
	return m_mouseButtons[button];
}

bool CInput::isButtonPressed(MouseButton button)
{
	return m_mouseButtonPressed[button];
}

bool CInput::isButtonReleased(MouseButton button)
{
	return m_mouseButtonReleased[button];
}

void CInput::getMousePos(int *x, int *y)
{
	POINT p;

	GetCursorPos(&p);

	if(ScreenToClient(m_hwnd, &p))
	{
		*x = p.x;
		*y = p.y;
	}
}

void CInput::setMousePos(int x, int y)
{
	POINT p;

	p.x = x;
	p.y = y;

	if(ClientToScreen(m_hwnd, &p))
	{
		SetCursorPos(p.x, p.y);
	}
}