#include "Camera.h"

using namespace ukl;

CCamera::CCamera()
	: m_rotX(0.0f)
	, m_rotY(0.0f)
	, m_rotZ(0.0f)
{
}


CCamera::~CCamera()
{
}

void CCamera::setPosition(float x, float y, float z)
{
	m_pos.x = x;
	m_pos.y = y;
	m_pos.z = z;
	return;
}


void CCamera::setRotation(float x, float y, float z)
{
	m_rotX = x;
	m_rotY = y;
	m_rotZ = z;
	return;
}

D3DXVECTOR3 CCamera::getPosition() const
{
	return m_pos;
}


D3DXVECTOR3 CCamera::getRotation() const
{
	return D3DXVECTOR3(m_rotX, m_rotY, m_rotZ);
}

void CCamera::render()
{
	D3DXVECTOR3 up, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_rotX * 0.0174532925f;
	yaw   = m_rotY * 0.0174532925f;
	roll  = m_rotZ * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = m_pos + lookAt;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_pos, &lookAt, &up);

	return;
}

void CCamera::getViewMatrix(D3DXMATRIX& viewMatrix) const
{
	viewMatrix = m_viewMatrix;
	return;
}

void CCamera::renderReflection(float height)
{
	D3DXVECTOR3 up, position, lookAt;
	float pitch, yaw, roll;
	D3DXMATRIX rotationMatrix;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = -1.0f;
	up.z = 0.0f;

	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	// Setup the position of the camera in the world.
	// For planar reflection invert the Y position of the camera.
	position.x = m_pos.x;
	position.y = -m_pos.y + (height * 2.0f);
	position.z = m_pos.z;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = -m_rotX * 0.0174532925f;
	yaw   = m_rotY * 0.0174532925f;
	roll  = m_rotZ * 0.0174532925f;	

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Setup where the camera is looking.
	//lookAt.x = sinf(yaw) + position.x;
	//lookAt.y = position.y;
	//lookAt.z = cosf(yaw) + position.y;

	lookAt = position + lookAt;

	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_reflectionViewMatrix, &position, &lookAt, &up);
}

D3DXMATRIX CCamera::getReflectionViewMatrix()
{
	return m_reflectionViewMatrix;
}