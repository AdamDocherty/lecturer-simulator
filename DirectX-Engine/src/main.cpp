//
// Author: Adam Docherty
// Date:   30-10-2013
//______________________________________________________________________________
// 
// Entry point for the program, creates and runs an instance of CSystem

#include "UKL.h"
#include "System.h"

using namespace ukl;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	CSystem *system;

	system = new CSystem;
	
	UKL_TRY_REPORT(system->initialise());
	UKL_TRY_REPORT(system->run());
	system->terminate();

	delete system;
	system = 0;

	return UKLE_RESULT_NO_ERROR;
}