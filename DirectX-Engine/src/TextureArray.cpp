#include "TextureArray.h"

using namespace ukl;

CTextureArray::CTextureArray(unsigned int size)
	: m_size(size)
{
	m_textures = new ID3D11ShaderResourceView*[m_size];
}

CTextureArray::~CTextureArray()
{
}

UKLResult CTextureArray::initialise(ID3D11Device* device, WCHAR* filename1, WCHAR* filename2)
{
	HRESULT result;


	// Load the first texture in.
	result = D3DX11CreateShaderResourceViewFromFile(device, filename1, NULL, NULL, &m_textures[0], NULL);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create shader resource view from  " << filename1);
	}

	if (filename2 != nullptr)
	{
		// Load the second texture in.
		result = D3DX11CreateShaderResourceViewFromFile(device, filename2, NULL, NULL, &m_textures[1], NULL);
		if(FAILED(result))
		{
			UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create shader resource view from " << filename2);
		}
	}

	return UKLE_RESULT_NO_ERROR;
}

void CTextureArray::terminate()
{
	unsigned int i;
	for (i=0;i<m_size;++i)
	{
		if(m_textures[i])
		{
			m_textures[i]->Release();
			m_textures[i] = NULL;
		}
	}
	
	return;
}


ID3D11ShaderResourceView** CTextureArray::getTextureArray()
{
	return m_textures;
}

