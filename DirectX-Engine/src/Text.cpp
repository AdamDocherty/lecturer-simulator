#include "Text.h"

using namespace ukl;

CText::CText()
	: m_font(0)
	, m_fontShader(0)
{
	m_font = 0;
	m_fontShader = 0;
}

CText::~CText()
{
}



UKLResult CText::initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, HWND hwnd, int screenWidth, int screenHeight, 
	D3DXMATRIX baseViewMatrix)
{
	// Store the screen width and height.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Store the base view matrix.
	m_baseViewMatrix = baseViewMatrix;

	// Create the font object.
	m_font = new CFont;

	// Initialize the font object.
	UKL_TRY(m_font->initialise(device, "data/fontdata.txt", L"data/font.dds"));

	// Create the font shader object.
	m_fontShader = new CFontShader;

	// Initialize the font shader object.
	UKL_TRY(m_fontShader->initialise(device, hwnd));

	// Create 3 sentences
	for (int i = 0; i < 3; ++i)
	{
		m_sentenceArray.push_back(0);
		UKL_TRY(initialiseSentence(&m_sentenceArray.back(), 16, device));
		UKL_TRY(updateSentence(m_sentenceArray.back(), "Hello world", 20, 20*(i+1), 1.0f, 1.0f, 0.0f, deviceContext));
	}

	return UKLE_RESULT_NO_ERROR;
}

void CText::terminate()
{
	for (auto it = m_sentenceArray.begin(); it != m_sentenceArray.end(); ++it)
	{
		releaseSentence(&(*it));
	}

	// Release the font shader object.
	UKL_TERMINATE_AND_DELETE(m_fontShader);

	// Release the font object.
	UKL_TERMINATE_AND_DELETE(m_font);
}

UKLResult CText::render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX orthoMatrix)
{
	for (auto it = m_sentenceArray.begin(); it != m_sentenceArray.end(); ++it)
	{
		UKL_TRY(renderSentence(deviceContext, *it, worldMatrix, orthoMatrix));
	}

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CText::updateText(unsigned int index, char* text, int x, int y, 
	float r, float g, float b, ID3D11DeviceContext* deviceContext)
{
	if (index >= m_sentenceArray.size())
	{
		UKL_ERROR(UKLE_RESULT_OUT_OF_BOUNDS, L"Attempt to access out of bounds member in Text");
	}

	UKL_TRY(updateSentence(m_sentenceArray[index], text, x, y, r, g, b, deviceContext));

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CText::initialiseSentence(SentenceType** sentence, int maxLength, ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;


	// Create a new sentence object.
	*sentence = new SentenceType;

	// Initialize the sentence buffers to null.
	(*sentence)->vertexBuffer = 0;
	(*sentence)->indexBuffer = 0;

	// Set the maximum length of the sentence.
	(*sentence)->maxLength = maxLength;

	// Set the number of vertices in the vertex array.
	(*sentence)->vertexCount = 6 * maxLength;

	// Set the number of indexes in the index array.
	(*sentence)->indexCount = (*sentence)->vertexCount;

	// Create the vertex array.
	vertices = new VertexType[(*sentence)->vertexCount];
	if(!vertices)
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create vertex array in Text");
	}

	// Create the index array.
	indices = new unsigned long[(*sentence)->indexCount];
	if(!indices)
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create index array in Text");
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * (*sentence)->vertexCount));

	// Initialize the index array.
	for(i=0; i<(*sentence)->indexCount; i++)
	{
		indices[i] = i;
	}

	// Set up the description of the dynamic vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * (*sentence)->vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &(*sentence)->vertexBuffer);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create vertex buffer in Text");
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * (*sentence)->indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &(*sentence)->indexBuffer);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create index buffer in Text");
	}

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	// Release the index array as it is no longer needed.
	delete [] indices;
	indices = 0;

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CText::updateSentence(SentenceType* sentence, char* text, int positionX, int positionY, float red, float green, float blue,
	ID3D11DeviceContext* deviceContext)
{
	int numLetters;
	VertexType* vertices;
	float drawX, drawY;
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	// Store the color of the sentence.
	sentence->red = red;
	sentence->green = green;
	sentence->blue = blue;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(text);

	// Check for possible buffer overflow.
	if(numLetters > sentence->maxLength)
	{
		UKL_ERROR(UKLE_RESULT_OUT_OF_BOUNDS, L"Buffer overflow while updating sentence");
	}

	// Create the vertex array.
	vertices = new VertexType[sentence->vertexCount];
	if(!vertices)
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not create vertex array");
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * sentence->vertexCount));

	// Calculate the X and Y pixel position on the screen to start drawing to.
	drawX = (float)(((m_screenWidth / 2) * -1) + positionX);
	drawY = (float)((m_screenHeight / 2) - positionY);

	// Use the font class to build the vertex array from the sentence text and sentence draw location.
	m_font->buildVertexArray((void*)vertices, text, drawX, drawY);

	// Lock the vertex buffer so it can be written to.
	result = deviceContext->Map(sentence->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Could not lock vertex buffer");
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * sentence->vertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(sentence->vertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	return UKLE_RESULT_NO_ERROR;
}

void CText::releaseSentence(SentenceType** sentence)
{
	if(*sentence)
	{
		// Release the sentence vertex buffer.
		if((*sentence)->vertexBuffer)
		{
			(*sentence)->vertexBuffer->Release();
			(*sentence)->vertexBuffer = 0;
		}

		// Release the sentence index buffer.
		if((*sentence)->indexBuffer)
		{
			(*sentence)->indexBuffer->Release();
			(*sentence)->indexBuffer = 0;
		}

		// Release the sentence.
		delete *sentence;
		*sentence = 0;
	}

	return;
}

UKLResult CText::renderSentence(ID3D11DeviceContext* deviceContext, SentenceType* sentence, D3DXMATRIX worldMatrix, 
	D3DXMATRIX orthoMatrix)
{
	unsigned int stride, offset;
	D3DXVECTOR4 pixelColor;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &sentence->vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create a pixel color vector with the input sentence color.
	pixelColor = D3DXVECTOR4(sentence->red, sentence->green, sentence->blue, 1.0f);

	// Render the text using the font shader.
	UKL_TRY(m_fontShader->render(deviceContext, sentence->indexCount, worldMatrix, m_baseViewMatrix, 
		orthoMatrix, m_font->getTexture(), pixelColor));

	return UKLE_RESULT_NO_ERROR;
}

