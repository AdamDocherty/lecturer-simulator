#include "Texture.h"

using namespace ukl;

CTexture::CTexture()
	: m_texture(0)
{
}


CTexture::~CTexture()
{
}

UKLResult CTexture::initialise(ID3D11Device* device, WCHAR* filename)
{
	HRESULT result;


	// Load the texture in.
	result = D3DX11CreateShaderResourceViewFromFile(device, filename, NULL, NULL, &m_texture, NULL);
	if(FAILED(result))
	{
		UKL_ERROR(UKLE_RESULT_FILE_NOT_FOUND, filename);
	}

	return UKLE_RESULT_NO_ERROR;
}

void CTexture::terminate()
{
	// Release the texture resource.
	if(m_texture)
	{
		m_texture->Release();
		m_texture = 0;
	}

	return;
}

ID3D11ShaderResourceView* CTexture::getTexture()
{
	return m_texture;
}