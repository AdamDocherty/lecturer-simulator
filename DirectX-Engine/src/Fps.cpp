//
// Author: Adam Docherty
// Date:   30-10-2013
//______________________________________________________________________________
//

#include "Fps.h"

using namespace ukl;

CFps::CFps()
{
}

CFps::~CFps()
{
}

UKLResult CFps::initialise()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
	return UKLE_RESULT_NO_ERROR;
}

void CFps::frame()
{
	++m_count;

	if(timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;

		m_startTime = timeGetTime();
	}
}

int CFps::getFps()
{
	return m_fps;
}