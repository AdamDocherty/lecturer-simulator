#include "FirstPersonCamera.h"

using namespace ukl;

CFirstPersonCamera::CFirstPersonCamera()
{
};

CFirstPersonCamera::~CFirstPersonCamera()
{
}

void CFirstPersonCamera::move(float x, float y, float z)
{
	m_pos.x += x;
	m_pos.y += y;
	m_pos.z += z;
}

void CFirstPersonCamera::moveForward(float x, float y, float z)
{
	D3DXVECTOR3 forward = D3DXVECTOR3(0.0f,0.0f,1.0f);
	D3DXVECTOR3 right = D3DXVECTOR3(1.0f,0.0f,0.0f);

	float yaw   = m_rotY * 0.0174532925f;

	D3DXMATRIX rotateYTempMatrix;
	D3DXMatrixRotationY(&rotateYTempMatrix, yaw);

	D3DXVec3TransformNormal(&right, &right, &rotateYTempMatrix);
	D3DXVec3TransformNormal(&forward, &forward, &rotateYTempMatrix);

	m_pos += x*right;
	m_pos += z*forward;
}

void CFirstPersonCamera::rotate(float x, float y, float z)
{
	m_rotX += x;
	if (m_rotX > 90.f)
		m_rotX = 90.f;
	if (m_rotX < -90.f)
		m_rotX = -90.f;

	m_rotY += y;
	m_rotZ += z;
}