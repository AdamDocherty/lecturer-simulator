#include "Skybox.h"

using namespace ukl;

CSkybox::CSkybox()
	: m_camera(NULL)
	, m_model(NULL)
{

}

CSkybox::~CSkybox()
{

}

void CSkybox::terminate()
{
	UKL_TERMINATE_AND_DELETE(m_model);
	UKL_DELETE(m_camera);
}

UKLResult CSkybox::initialise(ID3D11Device* device)
{
	m_model = new CModel;
	m_model->initialise(device, "assets/Skybox.txt", L"assets/Skybox01.dds");

	return UKLE_RESULT_NO_ERROR;
}

void CSkybox::setCamera(CCamera *camera)
{
	m_camera = camera;
}

void CSkybox::render()
{
	D3DXMATRIX *modelMatrix = m_model->getMatrix();
	D3DXVECTOR3 cameraPosition = m_camera->getPosition();
	D3DXMatrixTranslation(modelMatrix, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}

void CSkybox::renderReflection(float height)
{
	D3DXMATRIX *modelMatrix = m_model->getMatrix();
	D3DXVECTOR3 cameraPosition = m_camera->getPosition();
	D3DXMatrixTranslation(modelMatrix, cameraPosition.x, -cameraPosition.y + (height * 2), cameraPosition.z);
}

CModel * CSkybox::getModel()
{
	return m_model;
}