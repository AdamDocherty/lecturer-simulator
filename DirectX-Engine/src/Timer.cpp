#include "Timer.h"

using namespace ukl;

CTimer::CTimer()
{
}

CTimer::~CTimer()
{
}

UKLResult CTimer::initialise()
{
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_frequency);
	if(m_frequency == 0)
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Performance Query Failed");
	}

	// Find out how many times the frequency counter ticks every millisecond.
	m_ticksPerMs = (float)(m_frequency / 1000);

	QueryPerformanceCounter((LARGE_INTEGER*)&m_startTime);

	return UKLE_RESULT_NO_ERROR;
}

void CTimer::frame()
{
	INT64 currentTime;
	float timeDifference;


	QueryPerformanceCounter((LARGE_INTEGER*)& currentTime);

	timeDifference = (float)(currentTime - m_startTime);

	m_frameTime = timeDifference / m_ticksPerMs;

	m_startTime = currentTime;

	return;
}

float CTimer::getTime()
{
	return m_frameTime;
}

