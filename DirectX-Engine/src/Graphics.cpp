#include "Graphics.h"

using namespace ukl;

static const float FLOOR_Y_POS = -1.0f;

CGraphics::CGraphics()
	: m_d3d(NULL)
	, m_model(NULL)
	, m_floorModel(NULL)
	, m_wallModel(NULL)
	, m_textureShader(NULL)
	, m_lightShader(NULL)
	, m_bumpMapShader(NULL)
	, m_reflectionShader(NULL)
	, m_terrainShader(NULL)
	, m_tessellationShader(NULL)
	, m_toonShader(NULL)
	, m_outlineShader(NULL)
	, m_light(NULL)
	, m_text(NULL)
	, m_debugWindow(NULL)
	, m_debugWindowTexture(NULL)
	, m_waterHeight(2.75f)
	, m_waterTranslation(0.0f)
	, m_floorReflectionTexture(NULL)
	, m_rotation(0.0f)
	, m_fullScreenWindow(NULL)
	, m_sceneRenderTexture(NULL)
{
}

CGraphics::~CGraphics()
{
}

UKLResult CGraphics::initialise(CCamera* camera, int screenWidth, int screenHeight, HWND hwnd)
{
	m_camera = camera;
	m_camera->getViewMatrix(m_baseViewMatrix);
	m_camera->setPosition(-20.0f, 5.0f, 20.0f);
	m_camera->setRotation(6.0f, 135.0f, 0.0f);
	m_camera->render();

	m_d3d = new CD3D;
	UKL_TRY(m_d3d->initialise(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR));

	m_debugWindow = new CDebugWindow;
	UKL_TRY(m_debugWindow->initialise(m_d3d->getDevice(), screenWidth, screenHeight, screenWidth, screenHeight));

	m_bumpMapShader = new CBumpMapShader;
	UKL_TRY(m_bumpMapShader->initialise(m_d3d->getDevice(), hwnd));
	m_lightShader = new CLightShader;
	UKL_TRY(m_lightShader->initialise(m_d3d->getDevice(), hwnd));
	m_textureShader = new CTextureShader;
	UKL_TRY(m_textureShader->initialise(m_d3d->getDevice(), hwnd));
	m_reflectionShader = new CReflectionShader;
	UKL_TRY(m_reflectionShader->initialise(m_d3d->getDevice(), hwnd));
	m_terrainShader = new CTerrainShader;
	UKL_TRY(m_terrainShader->initialise(m_d3d->getDevice(), hwnd));
	m_tessellationShader = new CTessellationShader;
	UKL_TRY(m_tessellationShader->initialise(m_d3d->getDevice(), hwnd));
	m_toonShader = new CToonShader;
	UKL_TRY(m_toonShader->initialise(m_d3d->getDevice(), hwnd));
	m_outlineShader = new COutlineShader;
	UKL_TRY(m_outlineShader->initialise(m_d3d->getDevice(), hwnd));

	UKL_TRY(drawLoadingScreen());

	m_skybox = new CSkybox;
	UKL_TRY(m_skybox->initialise(m_d3d->getDevice()));
	m_skybox->setCamera(camera);

	m_model = new CModel;
	UKL_TRY(m_model->initialise(m_d3d->getDevice(), "assets/bath.txt", L"assets/stone01.dds", L"assets/stone01_bumpmap.dds"));

	m_floorModel = new CModel;
	UKL_TRY(m_floorModel->initialise(m_d3d->getDevice(), "assets/floor.txt", L"assets/marble01.dds", L"assets/marble01_bumpmap.dds"));
	D3DXMATRIX* floorMatrix = m_floorModel->getMatrix();
	D3DXMatrixTranslation(floorMatrix, 0.0f, FLOOR_Y_POS, 0.0f);

	m_wallModel = new CModel;
	UKL_TRY(m_wallModel->initialise(m_d3d->getDevice(), "assets/wall.txt", L"assets/wall01.dds", L"assets/wall01_bumpmap.dds"));
	D3DXMATRIX* wallMatrix = m_wallModel->getMatrix();
	D3DXMatrixTranslation(wallMatrix, 0.0f, 4.0f, 8.0f);

	m_waterModel = new CModel;
	UKL_TRY(m_waterModel->initialise(m_d3d->getDevice(), "assets/water.txt", L"assets/water01.dds", L"assets/water01.dds"));
	D3DXMATRIX* waterMatrix = m_waterModel->getMatrix();
	D3DXMatrixTranslation(waterMatrix, 0.0f, 0.0f, 0.0f);

	m_sceneRenderTexture = new CRenderTexture;
	UKL_TRY(m_sceneRenderTexture->initialise(m_d3d->getDevice(), screenWidth, screenHeight));
	m_fullScreenWindow = new CDebugWindow;
	UKL_TRY(m_fullScreenWindow->initialise(m_d3d->getDevice(), screenWidth, screenHeight, screenWidth, screenHeight));

	m_terrain = new CTerrain;
	UKL_TRY(m_terrain->initialise(m_d3d->getDevice(), "assets/heightmap.bmp", L"assets/ground01.dds"));
	D3DXMatrixTranslation(m_terrain->getMatrix(), -256.0f, -3.0f, -256.0f);
	m_blurTerrainTexture = new CTexture;
	UKL_TRY(m_blurTerrainTexture->initialise(m_d3d->getDevice(), L"assets/ground04.dds"));

	m_waterReflectionTexture = new CRenderTexture;
	UKL_TRY(m_waterReflectionTexture->initialise(m_d3d->getDevice(), screenWidth, screenHeight));
	m_waterRefractionTexture = new CRenderTexture;
	UKL_TRY(m_waterRefractionTexture->initialise(m_d3d->getDevice(), screenWidth, screenHeight));

	m_light = new CLight;
	m_light->setDiffuseColour(1.0f, 1.0f, 1.0f, 1.0f);
	m_light->setDirection(1.0f, -1.0f, -1.0f);

	m_text = new CText;
	UKL_TRY(m_text->initialise(m_d3d->getDevice(), m_d3d->getDeviceContext(),
		hwnd, screenWidth, screenHeight, m_baseViewMatrix));

	m_debugWindowTexture = new CRenderTexture;
	UKL_TRY(m_debugWindowTexture->initialise(m_d3d->getDevice(), screenWidth, screenHeight));

	m_floorReflectionTexture = new CRenderTexture;
	UKL_TRY(m_floorReflectionTexture->initialise(m_d3d->getDevice(), screenWidth, screenHeight));

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::drawLoadingScreen()
{
	m_camera->render();

	D3DXMATRIX worldMatrix, orthoMatrix;

	m_d3d->getWorldMatrix(worldMatrix);
	m_d3d->getOrthoMatrix(orthoMatrix);

	CTexture *loadingScreen = new CTexture;
	UKL_TRY(loadingScreen->initialise(m_d3d->getDevice(), L"assets/abertay-lecturer-simulator.dds"));

	UKL_TRY(m_debugWindow->render(m_d3d->getDeviceContext(), 0, 0));

	m_d3d->beginScene(0.0f, 0.0f, 0.2f, 1.0f);

	UKL_TRY(m_textureShader->render(m_d3d->getDeviceContext(), m_debugWindow->getIndexCount(), worldMatrix, m_baseViewMatrix,
		orthoMatrix, loadingScreen->getTexture()));

	m_d3d->endScene();

	loadingScreen->terminate();
	delete loadingScreen;
	loadingScreen = 0;

	return UKLE_RESULT_NO_ERROR;
}

void CGraphics::terminate()
{
	UKL_TERMINATE_AND_DELETE(m_text);
	UKL_DELETE(m_light);
	UKL_TERMINATE_AND_DELETE(m_reflectionShader);
	UKL_TERMINATE_AND_DELETE(m_waterReflectionTexture);
	UKL_TERMINATE_AND_DELETE(m_waterRefractionTexture);
	UKL_TERMINATE_AND_DELETE(m_floorReflectionTexture);
	UKL_TERMINATE_AND_DELETE(m_debugWindowTexture);
	UKL_TERMINATE_AND_DELETE(m_debugWindow);
	UKL_TERMINATE_AND_DELETE(m_bumpMapShader);
	UKL_TERMINATE_AND_DELETE(m_lightShader);
	UKL_TERMINATE_AND_DELETE(m_textureShader);
	UKL_TERMINATE_AND_DELETE(m_tessellationShader);
	UKL_TERMINATE_AND_DELETE(m_toonShader);
	UKL_TERMINATE_AND_DELETE(m_outlineShader);
	UKL_TERMINATE_AND_DELETE(m_waterModel);
	UKL_TERMINATE_AND_DELETE(m_wallModel);
	UKL_TERMINATE_AND_DELETE(m_floorModel);
	UKL_TERMINATE_AND_DELETE(m_model);
	UKL_TERMINATE_AND_DELETE(m_d3d);
}

UKLResult CGraphics::frame(CCamera *camera, int fps, int cpuUsage, float timeElapsed)
{
	m_camera = camera;
	m_paulVision = (GetKeyState('T') > 0);

	char fps_str[16];
	sprintf_s(fps_str, "%d", fps);
	UKL_TRY(m_text->updateText(0, fps_str, 20, 20, 0.1f, 1.0f, 0.1f, m_d3d->getDeviceContext()));

	char cpuUsage_str[16];
	sprintf_s(cpuUsage_str, "%d", cpuUsage);
	UKL_TRY(m_text->updateText(1, cpuUsage_str, 20, 40, 1.0f, 0.1f, 0.1f, m_d3d->getDeviceContext()));

	char timeElapsed_str[16];
	sprintf_s(timeElapsed_str, "%f", timeElapsed);
	UKL_TRY(m_text->updateText(2, timeElapsed_str, 20, 60, 1.0f, 1.0f, 1.0f, m_d3d->getDeviceContext()));

	m_rotation += (float)D3DX_PI * timeElapsed * 1.0f;
	if(m_rotation > 360.0f)
	{
		m_rotation -= 360.0f;
	}

	m_waterTranslation += 0.001f;
	if(m_waterTranslation > 1.0f)
	{
		m_waterTranslation -= 1.0f;
	}

	UKL_TRY(render());
	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::render()
{
	D3DXMATRIX projectionMatrix, worldMatrix, orthoMatrix;

	//UKL_TRY(renderToWaterRefraction());
	//UKL_TRY(renderToWaterReflection());

	// Only render the floor reflection if it will actually be seen.
	if (m_camera->getPosition().y > FLOOR_Y_POS)
		UKL_TRY(renderToFloorReflection());

	//UKL_TRY(renderToDebugWindow());
	UKL_TRY(renderSceneToTexture());

	m_d3d->beginScene(0.0f, 0.0f, 0.2f, 1.0f);

	UKL_TRY(renderScene());

	m_d3d->turnZBufferOff();

	UKL_TRY(m_fullScreenWindow->render(m_d3d->getDeviceContext(), 0, 0));
	UKL_TRY(m_debugWindow->render(m_d3d->getDeviceContext(), 0, 0));

	m_d3d->getWorldMatrix(worldMatrix);
	m_d3d->getOrthoMatrix(orthoMatrix);

	// Render the debug window
	//UKL_TRY(m_textureShader->render(m_d3d->getDeviceContext(), m_debugWindow->getIndexCount(), worldMatrix, m_baseViewMatrix,
	//				 orthoMatrix, m_debugWindowTexture->getShaderResourceView()));

	//UKL_TRY(m_textureShader->render(m_d3d->getDeviceContext(), m_fullScreenWindow->getIndexCount(), worldMatrix, m_baseViewMatrix,
		//orthoMatrix, m_debugWindowTexture->getShaderResourceView()));

	// Enable alpha blending to draw the text
	m_d3d->turnAlphaBlendingOn();

	UKL_TRY(m_text->render(m_d3d->getDeviceContext(), worldMatrix, orthoMatrix));

	m_d3d->turnAlphaBlendingOff();
	m_d3d->turnZBufferOn();

	// Present the rendered scene to the screen.
	m_d3d->endScene(); 

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderToDebugWindow()
{
	// Set the render target to be the render to texture.
	m_debugWindowTexture->setRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView());

	// Clear the render to texture.
	m_debugWindowTexture->clearRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView(), 0.1f, 0.5f, 0.1f, 1.0f);

	// Render the scene now and it will draw to the render to texture instead of the back buffer.
	UKL_TRY(renderScene());

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_d3d->setBackBufferRenderTarget();

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderSceneToTexture()
{
	m_sceneRenderTexture->setRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView());

	m_sceneRenderTexture->clearRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);

	UKL_TRY(renderScene());

	m_d3d->setBackBufferRenderTarget();

	m_d3d->resetViewport();

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderScene()
{
	D3DXMATRIX projectionMatrix, viewMatrix, worldMatrix, reflectionMatrix;

	D3DXVECTOR4 ambientColour(0.2f, 0.2f, 0.2f, 1.0f);
	float		specularPower = 32.0f;
	D3DXVECTOR4 specularColour(0.0f, 1.0f, 1.0f, 1.0f);
	D3DXVECTOR4 toonCutoff(0.90f, 0.5f, 0.05f, 0.0f);

	m_camera->render();

	m_camera->getViewMatrix(viewMatrix);
	m_d3d->getWorldMatrix(worldMatrix);
	m_d3d->getProjectionMatrix(projectionMatrix);

	m_d3d->turnZBufferOff();

	CModel * skyboxModel = m_skybox->getModel();
	m_skybox->render();
	skyboxModel->render(m_d3d->getDeviceContext());
	
	m_textureShader->render(m_d3d->getDeviceContext(), skyboxModel->getIndexCount(), *(skyboxModel->getMatrix()),
		viewMatrix, projectionMatrix, *(skyboxModel->getTextureArray()));

	m_d3d->turnZBufferOn();

	m_terrain->render(m_d3d->getDeviceContext());

	if (m_paulVision)
	{
		UKL_TRY(m_toonShader->render(m_d3d->getDeviceContext(), m_terrain->getIndexCount(), *(m_terrain->getMatrix()),
			viewMatrix, projectionMatrix, m_blurTerrainTexture->getTexture(), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour(), m_camera->getPosition(), toonCutoff));

		m_d3d->turnFrontfaceCullingOn();

		UKL_TRY(m_outlineShader->render(m_d3d->getDeviceContext(), m_terrain->getIndexCount(),  *(m_terrain->getMatrix()), viewMatrix,
			projectionMatrix, D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f), 0.3f));

		m_d3d->turnFrontfaceCullingOff();
	}
	else
	{
		UKL_TRY(m_terrainShader->render(m_d3d->getDeviceContext(), m_terrain->getIndexCount(), *(m_terrain->getMatrix()), viewMatrix, 
			projectionMatrix, ambientColour, m_light->getDiffuseColour(), m_light->getDirection(), m_terrain->getTexture()));
	}
	

	D3DXMATRIX* modelMatrix = m_model->getMatrix();
	D3DXMatrixIdentity(modelMatrix);

	m_model->render(m_d3d->getDeviceContext());

	if (m_paulVision)
	{
		UKL_TRY(m_toonShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(), *(m_model->getMatrix()),
			viewMatrix, projectionMatrix, *(m_model->getTextureArray()), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour(), m_camera->getPosition(), toonCutoff));

		m_d3d->turnFrontfaceCullingOn();

		UKL_TRY(m_outlineShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(),  *(m_model->getMatrix()), viewMatrix,
			projectionMatrix, D3DXVECTOR4(1.0f, 0.0f, 1.0f, 1.0f), 0.1f));

		m_d3d->turnFrontfaceCullingOff();
	}
	else
	{
		UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(),	 m_model->getIndexCount(), *(m_model->getMatrix()),
			viewMatrix, projectionMatrix, m_model->getTextureArray(), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour()));
	}

	D3DXMATRIX translationMatrix, rotationMatrix, scaleMatrix;
	D3DXMatrixTranslation(&translationMatrix, 10.0f, -0.5f, 0.0f);
	D3DXMatrixRotationY(&rotationMatrix, m_rotation);
	D3DXMatrixScaling(&scaleMatrix, 0.5f, 0.5f, 0.5f);

	D3DXMatrixMultiply(modelMatrix, modelMatrix, &scaleMatrix);
	D3DXMatrixMultiply(modelMatrix, modelMatrix, &translationMatrix);
	D3DXMatrixMultiply(modelMatrix, modelMatrix, &rotationMatrix);

	if (m_paulVision)
	{
		UKL_TRY(m_toonShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(), *(m_model->getMatrix()),
			viewMatrix, projectionMatrix, *(m_model->getTextureArray()), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour(), m_camera->getPosition(), toonCutoff));

		m_d3d->turnFrontfaceCullingOn();

		UKL_TRY(m_outlineShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(),  *(m_model->getMatrix()), viewMatrix,
			projectionMatrix, D3DXVECTOR4(0.0f, 1.0f, 1.0f, 1.0f), 0.1f));

		m_d3d->turnFrontfaceCullingOff();
	}
	else
	{
		UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(),	 m_model->getIndexCount(), *(m_model->getMatrix()),
			viewMatrix, projectionMatrix, m_model->getTextureArray(), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour()));
	}

	m_wallModel->render(m_d3d->getDeviceContext());

	if (m_paulVision)
	{
		UKL_TRY(m_toonShader->render(m_d3d->getDeviceContext(), m_wallModel->getIndexCount(), *(m_wallModel->getMatrix()),
			viewMatrix, projectionMatrix, *(m_wallModel->getTextureArray()), m_light->getDirection(), ambientColour, 
			m_light->getDiffuseColour(), m_camera->getPosition(), toonCutoff));

		m_d3d->turnFrontfaceCullingOn();

		UKL_TRY(m_outlineShader->render(m_d3d->getDeviceContext(), m_wallModel->getIndexCount(),  *(m_wallModel->getMatrix()), viewMatrix,
			projectionMatrix, D3DXVECTOR4(1.0f, 1.0f, 0.0f, 1.0f), 0.1f));

		m_d3d->turnFrontfaceCullingOff();
	}
	else
	{
		UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(), *(m_wallModel->getMatrix()),
			viewMatrix, projectionMatrix, m_wallModel->getTextureArray(), m_light->getDirection(), ambientColour,
			m_light->getDiffuseColour()));
	}

	D3DXMATRIX* floorMatrix = m_floorModel->getMatrix();
	D3DXMatrixTranslation(floorMatrix, 0.0f, FLOOR_Y_POS, 0.0f);

	reflectionMatrix = m_camera->getReflectionViewMatrix();

	/* Render Floor with reflection shader */
	m_floorModel->render(m_d3d->getDeviceContext());
	UKL_TRY(m_reflectionShader->render(m_d3d->getDeviceContext(), m_floorModel->getIndexCount(), *(m_floorModel->getMatrix()), viewMatrix, 
		projectionMatrix, *(m_floorModel->getTextureArray()), m_floorReflectionTexture->getShaderResourceView(), reflectionMatrix));
	/* End Render floor with reflectionShader */

	/* Render Floor with tessellation shader *
	m_floorModel->render(m_d3d->getDeviceContext(), true);
	UKL_TRY(m_tessellationShader->render(m_d3d->getDeviceContext(), m_floorModel->getIndexCount(), *(m_floorModel->getMatrix()), viewMatrix,
		projectionMatrix, 8));
	/* End Render floor with tessellation shader */

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderToFloorReflection()
{
	D3DXMATRIX worldMatrix, reflectionViewMatrix, projectionMatrix;

	D3DXVECTOR4 ambientColour(0.2f, 0.2f, 0.2f, 1.0f);
	float		specularPower = 32.0f;
	D3DXVECTOR4 specularColour(0.0f, 1.0f, 1.0f, 1.0f);

	m_floorReflectionTexture->setRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView());

	// Clear the render to texture.
	m_floorReflectionTexture->clearRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);

	// Use the camera to calculate the reflection matrix.
	m_camera->renderReflection(FLOOR_Y_POS);
	
	// Get the camera reflection view matrix instead of the normal view matrix.
	reflectionViewMatrix = m_camera->getReflectionViewMatrix();

	// Get the world and projection matrices.
	m_d3d->getWorldMatrix(worldMatrix);
	m_d3d->getProjectionMatrix(projectionMatrix);

	m_d3d->getWorldMatrix(worldMatrix);
	m_d3d->getProjectionMatrix(projectionMatrix);

	m_d3d->turnZBufferOff();

	CModel * skyboxModel = m_skybox->getModel();
	m_skybox->renderReflection(FLOOR_Y_POS);
	skyboxModel->render(m_d3d->getDeviceContext());

	m_textureShader->render(m_d3d->getDeviceContext(), skyboxModel->getIndexCount(), *(skyboxModel->getMatrix()),
		reflectionViewMatrix, projectionMatrix, *(skyboxModel->getTextureArray()));

	m_d3d->turnZBufferOn();

	m_terrain->render(m_d3d->getDeviceContext());
	UKL_TRY(m_terrainShader->render(m_d3d->getDeviceContext(), m_terrain->getIndexCount(), *(m_terrain->getMatrix()), reflectionViewMatrix, 
		projectionMatrix, ambientColour, m_light->getDiffuseColour(), m_light->getDirection(), m_terrain->getTexture()));

	D3DXMATRIX* modelMatrix = m_model->getMatrix();
	D3DXMatrixIdentity(modelMatrix);

	m_model->render(m_d3d->getDeviceContext());
	//D3DXMatrixTranslation(modelMatrix, 0, 70.0f, 0);

	//UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(),	 m_model->getIndexCount(), *(m_model->getMatrix()),
	//	viewMatrix, projectionMatrix, m_model->getTextureArray(), m_light->getDirection(), ambientColour, 
	//	m_light->getDiffuseColour()));

	UKL_TRY(m_lightShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(), *(m_model->getMatrix()),
	reflectionViewMatrix, projectionMatrix, *(m_model->getTextureArray()), m_light->getDirection(), ambientColour, 
	m_light->getDiffuseColour(), m_camera->getPosition(), specularColour, specularPower));

	D3DXMATRIX translationMatrix, rotationMatrix, scaleMatrix;
	D3DXMatrixTranslation(&translationMatrix, 10.0f, -0.5f, 0.0f);
	D3DXMatrixRotationY(&rotationMatrix, m_rotation);
	D3DXMatrixScaling(&scaleMatrix, 0.5f, 0.5f, 0.5f);

	D3DXMatrixMultiply(modelMatrix,  &scaleMatrix, &translationMatrix);
	D3DXMatrixMultiply(modelMatrix, modelMatrix, &rotationMatrix);

	UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(),	 m_model->getIndexCount(), *(m_model->getMatrix()),
	reflectionViewMatrix, projectionMatrix, m_model->getTextureArray(), m_light->getDirection(), ambientColour, 
	m_light->getDiffuseColour()));

	m_wallModel->render(m_d3d->getDeviceContext());

	UKL_TRY(m_bumpMapShader->render(m_d3d->getDeviceContext(), m_model->getIndexCount(), *(m_wallModel->getMatrix()),
		reflectionViewMatrix, projectionMatrix, m_wallModel->getTextureArray(), m_light->getDirection(), ambientColour,
		m_light->getDiffuseColour()));

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_d3d->setBackBufferRenderTarget();

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderToWaterReflection()
{
	D3DXVECTOR4 clipPlane;
	D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix;

	// Setup a clipping plane based on the height of the water to clip everything above it.
	clipPlane = D3DXVECTOR4(0.0f, -1.0f, 0.0f, m_waterHeight + 0.1f);

	// Set the render target to be the refraction render to texture.
	m_waterRefractionTexture->setRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView());

	// Clear the refraction render to texture.
	m_waterRefractionTexture->clearRenderTarget(m_d3d->getDeviceContext(), m_d3d->getDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_camera->render();

	// Get the world, view, and projection matrices from the camera and d3d objects.
	m_d3d->getWorldMatrix(worldMatrix);
	m_camera->getViewMatrix(viewMatrix);
	m_d3d->getProjectionMatrix(projectionMatrix);

	// Translate to where the bath model will be rendered.
	//D3DXMatrixTranslation(&worldMatrix, 0.0f, 2.0f, 0.0f);

	// Put the bath model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	m_model->render(m_d3d->getDeviceContext());

	// Render the bath model using the light shader.
	//result = m_refractionShader->render(m_d3d->getDeviceContext(), m_bathModel->GetIndexCount(), worldMatrix, viewMatrix,
	//				    projectionMatrix, m_BathModel->GetTexture(), m_Light->GetDirection(), 
	//				    m_Light->GetAmbientColor(), m_Light->GetDiffuseColor(), clipPlane);
	//if(!result)
	//{
	//	return false;
	//}

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_d3d->setBackBufferRenderTarget();

	return UKLE_RESULT_NO_ERROR;
}

UKLResult CGraphics::renderToWaterRefraction()
{
	return UKLE_RESULT_NO_ERROR;
}