#include "Font.h"

using namespace ukl;

CFont::CFont()
	: m_font(0)
	, m_texture(0)
{
}

CFont::~CFont()
{
}

UKLResult CFont::initialise(ID3D11Device* device, char* fontFilename, WCHAR* textureFilename)
{
	// Load in the text file containing the font data.
	UKL_TRY(loadFontData(fontFilename));

	// Load the texture that has the font characters on it.
	UKL_TRY(loadTexture(device, textureFilename));

	return UKLE_RESULT_NO_ERROR;
}

void CFont::terminate()
{
	// Release the font texture.
	releaseTexture();

	// Release the font data.
	releaseFontData();
}

UKLResult CFont::loadFontData(char* filename)
{
	std::ifstream fin;
	int i;
	char temp;

	// Create the font spacing buffer.
	m_font = new FontType[95];
	if(!m_font)
	{
		UKL_ERROR(UKLE_RESULT_UNKNOWN_ERROR, L"Couldn't create font while loading font data");
	}

	// Read in the font size and spacing between chars.
	fin.open(filename);
	if(fin.fail())
	{
		const size_t cSize = strlen(filename)+1;
		wchar_t* wc = new wchar_t[cSize];
		size_t strLength = 0;
		mbstowcs_s (&strLength, wc, cSize, filename, cSize - 1);
		// errno_t mbstowcs_s(size_t * _PtNumOfCharConverted, wchar_t * _DstBuf, size_t _SizeInWords, const char * _SrcBuf, size_t _MaxCount)
		UKL_ERROR(UKLE_RESULT_FILE_NOT_FOUND, wc)
	}

	// Read in the 95 used ascii characters for text.
	for(i=0; i<95; i++)
	{
		fin.get(temp);
		while(temp != ' ')
		{
			fin.get(temp);
		}
		fin.get(temp);
		while(temp != ' ')
		{
			fin.get(temp);
		}

		fin >> m_font[i].left;
		fin >> m_font[i].right;
		fin >> m_font[i].size;
	}

	// Close the file.
	fin.close();

	return UKLE_RESULT_NO_ERROR;
}

void CFont::releaseFontData()
{
	// Release the font data array.
	if(m_font)
	{
		delete [] m_font;
		m_font = 0;
	}
}

UKLResult CFont::loadTexture(ID3D11Device* device, WCHAR* filename)
{
	// Create the texture object.
	m_texture = new CTexture;

	// Initialize the texture object.
	UKL_TRY(m_texture->initialise(device, filename));

	return UKLE_RESULT_NO_ERROR;
}

void CFont::releaseTexture()
{
	// Release the texture object.
	UKL_TERMINATE_AND_DELETE(m_texture);
}

ID3D11ShaderResourceView* CFont::getTexture()
{
	return m_texture->getTexture();
}

void CFont::buildVertexArray(void* vertices, char* sentence, float drawX, float drawY)
{
	VertexType* vertexPtr;
	int numLetters, index, i, letter;


	// Coerce the input vertices into a VertexType structure.
	vertexPtr = (VertexType*)vertices;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(sentence);

	// Initialize the index to the vertex array.
	index = 0;

	// Draw each letter onto a quad.
	for(i=0; i<numLetters; i++)
	{
		letter = ((int)sentence[i]) - 32;

		// If the letter is a space then just move over three pixels.
		if(letter == 0)
		{
			drawX = drawX + 3.0f;
		}
		else
		{
			// First triangle in quad.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].left, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3((drawX + m_font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].right, 1.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3(drawX, (drawY - 16), 0.0f);  // Bottom left.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].left, 1.0f);
			index++;

			// Second triangle in quad.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].left, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3(drawX + m_font[letter].size, drawY, 0.0f);  // Top right.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].right, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3((drawX + m_font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].texture = D3DXVECTOR2(m_font[letter].right, 1.0f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + m_font[letter].size + 1.0f;
		}
	}
}

